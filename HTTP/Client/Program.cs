﻿using System;
using System.Net.Http;

namespace Client
{
    class Program
    {
        static void Main(string[] args)
        {
            PostData();
            GetData();
            Console.Read();
        }

        static async void GetData()
        {
            var client = new HttpClient();
            var json = await client.GetStringAsync("http://localhost/contacts");
            System.Console.WriteLine(json);
        }

        static async void PostData()
        {
            var client = new HttpClient();
            var json = await client.PostAsync("http://localhost/contacts", new StringContent("{'first_name':'Giulio','last_name':'Ennor','email':'gennor2@businessinsider.com','gender':'Male','ip_address':'175.43.181.109'}"));
            System.Console.WriteLine(json);
        }
    }
}
