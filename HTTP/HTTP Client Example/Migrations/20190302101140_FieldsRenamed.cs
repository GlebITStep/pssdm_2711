﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace HTTP_Client_Example.Migrations
{
    public partial class FieldsRenamed : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "gender",
                table: "Contacts",
                newName: "Gender");

            migrationBuilder.RenameColumn(
                name: "email",
                table: "Contacts",
                newName: "Email");

            migrationBuilder.RenameColumn(
                name: "id",
                table: "Contacts",
                newName: "Id");

            migrationBuilder.RenameColumn(
                name: "last_name",
                table: "Contacts",
                newName: "LastName");

            migrationBuilder.RenameColumn(
                name: "ip_address",
                table: "Contacts",
                newName: "IpAddress");

            migrationBuilder.RenameColumn(
                name: "first_name",
                table: "Contacts",
                newName: "FirstName");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Gender",
                table: "Contacts",
                newName: "gender");

            migrationBuilder.RenameColumn(
                name: "Email",
                table: "Contacts",
                newName: "email");

            migrationBuilder.RenameColumn(
                name: "Id",
                table: "Contacts",
                newName: "id");

            migrationBuilder.RenameColumn(
                name: "LastName",
                table: "Contacts",
                newName: "last_name");

            migrationBuilder.RenameColumn(
                name: "IpAddress",
                table: "Contacts",
                newName: "ip_address");

            migrationBuilder.RenameColumn(
                name: "FirstName",
                table: "Contacts",
                newName: "first_name");
        }
    }
}
