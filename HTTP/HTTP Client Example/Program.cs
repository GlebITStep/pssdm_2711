﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using HTTP_Client_Example.Models;
using Newtonsoft.Json;

namespace HTTP_Client_Example
{
class Program
    {        
        static void Main(string[] args)
        {
            StartServer();
            Console.Read();
        }

        private static async void StartServer()
        {
            var server = new HttpListener();
            server.Prefixes.Add("http://*:80/");
            server.Start();
            System.Console.WriteLine("Server started...");

            while (true)
            {
                var context = await server.GetContextAsync();
                var request = context.Request;
                var response = context.Response;

                System.Console.WriteLine($"{DateTime.Now} - {request.HttpMethod} - {request.RawUrl}");
                var stream = new StreamWriter(context.Response.OutputStream);
                var dbcontext = new ContactsDbContext();

                if (request.RawUrl == "/favicon.ico")
                {
                    using (var binaryStream = new BinaryWriter(context.Response.OutputStream))
                    {
                        var bytes = await File.ReadAllBytesAsync("cat.ico");
                        stream.Write(bytes);
                        continue;
                    } 
                }
                else if(request.RawUrl == "/")
                {
                    var html = await File.ReadAllTextAsync("index.html");
                    await stream.WriteLineAsync(html);
                }
                else if(request.RawUrl.StartsWith("/contacts"))
                {
                    if (request.RawUrl == "/contacts" && request.HttpMethod == "GET")
                    {
                        var contacts = dbcontext.Contacts.ToList();
                        await stream.WriteLineAsync(JsonConvert.SerializeObject(contacts));
                    }
                    else if(request.HttpMethod == "GET")
                    {       
                        // int.TryParse(request.QueryString["id"], out int id);

                        var httpParams = request.RawUrl.Split('/', options: StringSplitOptions.RemoveEmptyEntries);
                        int.TryParse(httpParams[1], out int id);

                        var contact = dbcontext.Contacts.FirstOrDefault(x => x.Id == id);
                        
                        if (contact != null)
                            await stream.WriteLineAsync(JsonConvert.SerializeObject(contact));
                        else
                            await stream.WriteLineAsync("Error!");                        
                    }
                    else if(request.HttpMethod == "POST")
                    {
                        using (var reader = new StreamReader(request.InputStream))
                        {
                            var json = await reader.ReadToEndAsync();
                            var contact = JsonConvert.DeserializeObject<Contact>(json);
                            
                            dbcontext.Add(contact);
                            await dbcontext.SaveChangesAsync();
                        }      
                        await stream.WriteLineAsync("Done!");      
                    }
                    else if(request.HttpMethod == "DELETE")
                    {
                        var httpParams = request.RawUrl.Split('/', options: StringSplitOptions.RemoveEmptyEntries);
                        int.TryParse(httpParams[1], out int id);

                        dbcontext.Contacts.Remove(dbcontext.Contacts.FirstOrDefault(x => x.Id == id));
                        await dbcontext.SaveChangesAsync();

                        await stream.WriteLineAsync("Done!");      
                    }
                }

                stream.Close();
                dbcontext.Dispose();
            }
        }
    }


    // class Program
    // {        
    //     static void Main(string[] args)
    //     {
    //         StartServer();
    //         Console.Read();
    //     }

    //     private static async void StartServer()
    //     {
    //         var server = new HttpListener();
    //         server.Prefixes.Add("http://*:80/");
    //         server.Start();
    //         System.Console.WriteLine("Server started...");

    //         while (true)
    //         {
    //             var context = await server.GetContextAsync();
    //             var request = context.Request;
    //             var response = context.Response;

    //             System.Console.WriteLine($"{DateTime.Now} - {request.HttpMethod} - {request.RawUrl}");
    //             var stream = new StreamWriter(context.Response.OutputStream);

    //             if (request.RawUrl == "/favicon.ico")
    //             {
    //                 using (var binaryStream = new BinaryWriter(context.Response.OutputStream))
    //                 {
    //                     var bytes = await File.ReadAllBytesAsync("cat.ico");
    //                     stream.Write(bytes);
    //                     continue;
    //                 } 
    //             }
    //             else if(request.RawUrl == "/")
    //             {
    //                 var html = await File.ReadAllTextAsync("index.html");
    //                 await stream.WriteLineAsync(html);
    //             }
    //             else if(request.RawUrl.StartsWith("/contacts"))
    //             {
    //                 if (request.RawUrl == "/contacts" && request.HttpMethod == "GET")
    //                 {
    //                     var json = await File.ReadAllTextAsync("data.json");
    //                     await stream.WriteLineAsync(json);
    //                 }
    //                 else if(request.HttpMethod == "GET")
    //                 {       
    //                     // int.TryParse(request.QueryString["id"], out int id);

    //                     var httpParams = request.RawUrl.Split('/', options: StringSplitOptions.RemoveEmptyEntries);
    //                     int.TryParse(httpParams[1], out int id);

    //                     var json = await File.ReadAllTextAsync("data.json");
    //                     var contacts = JsonConvert.DeserializeObject<List<Contact>>(json); 
    //                     var contact = contacts.FirstOrDefault(x => x.id == id);
                        
    //                     if (contact != null)
    //                         await stream.WriteLineAsync(JsonConvert.SerializeObject(contact));
    //                     else
    //                         await stream.WriteLineAsync("Error!");                        
    //                 }
    //                 else if(request.HttpMethod == "POST")
    //                 {
    //                     using (var reader = new StreamReader(request.InputStream))
    //                     {
    //                         var json = await reader.ReadToEndAsync();
    //                         var contact = JsonConvert.DeserializeObject<Contact>(json);
    //                         var jsonAll = await File.ReadAllTextAsync("data.json");
    //                         var contacts = JsonConvert.DeserializeObject<List<Contact>>(jsonAll);
    //                         contacts.Add(contact);
    //                         var jsonResult = JsonConvert.SerializeObject(contacts);
    //                         await File.WriteAllTextAsync("data.json", jsonResult);
    //                         await stream.WriteLineAsync("Done!");
    //                     }            
    //                 }
    //             }

    //             stream.Close();
    //         }
    //     }
    // }
}
