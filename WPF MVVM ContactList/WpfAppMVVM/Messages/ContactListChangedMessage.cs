﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfAppMVVM.Models;

namespace WpfAppMVVM.Messages
{
    class ContactListChangedMessage
    {
        public Contact Item { get; set; }
    }
}
