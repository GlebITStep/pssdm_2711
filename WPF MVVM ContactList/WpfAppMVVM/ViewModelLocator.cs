﻿using Autofac;
using Autofac.Configuration;
using GalaSoft.MvvmLight;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using WpfAppMVVM.Services;
using WpfAppMVVM.ViewModels;
using WpfAppMVVM.Views;

namespace WpfAppMVVM
{
    class ViewModelLocator
    {
        private AppViewModel appViewModel;
        private ContactListViewModel contactListViewModel;
        private AddEditContactViewModel editContactViewModel;

        private INavigationService navigationService;

        public static IContainer Container;

        public ViewModelLocator()
        {
            try
            {
                var config = new ConfigurationBuilder();
                config.AddJsonFile("autofac.json");
                var module = new ConfigurationModule(config.Build());
                var builder = new ContainerBuilder();
                builder.RegisterModule(module);
                Container = builder.Build();

                navigationService = Container.Resolve<INavigationService>();
                appViewModel = Container.Resolve<AppViewModel>();
                contactListViewModel = Container.Resolve<ContactListViewModel>();
                editContactViewModel = Container.Resolve<AddEditContactViewModel>();

                navigationService.Register<ContactListView>(contactListViewModel);
                navigationService.Register<AddEditContactView>(editContactViewModel);

                navigationService.Navigate<ContactListView>();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public ViewModelBase GetAppViewModel()
        {
            return appViewModel;
        }
    }
}
