﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdoNetTest
{
    class Product
    {
        public int Id { get; set; }
        public string ProductName { get; set; }
        public decimal UnitPrice { get; set; }
        public string Package { get; set; }
        public int SupplierId { get; set; }
        public bool IsDiscounted { get; set; }

        public override string ToString()
        {
            return $"{Id} {ProductName} {UnitPrice} {Package} {SupplierId} {IsDiscounted}"; 
        }
    }

    class Program
    {
        static List<Product> list = new List<Product>();

        static void Main(string[] args)
        {
            var connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;

            using (var connection = new SqlConnection(connectionString))
            {
                try
                {
                    connection.Open();
                    var query = "select * from Product where UnitPrice < 10";
                    var command = new SqlCommand(query, connection);
                    var reader = command.ExecuteReader();
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            var product = new Product();
                            product.Id = reader.GetInt32(0);
                            product.ProductName = reader.GetString(1);
                            product.SupplierId = reader.GetInt32(2);
                            product.UnitPrice = reader.GetDecimal(3);
                            product.Package = reader.GetString(4);
                            product.IsDiscounted = reader.GetBoolean(5);
                            list.Add(product);
                        }
                    }
                    else
                    {
                        Console.WriteLine("No results!");
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }

            foreach (var item in list)
            {
                Console.WriteLine(item);
            }

            Console.ReadKey();
        }
    }
}
