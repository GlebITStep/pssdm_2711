﻿using IPluginDll;
using MyLib;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ConsoleAppDllTest
{
    using HWND = IntPtr;

    /// <summary>Contains functionality to get all the open windows.</summary>
    public static class OpenWindowGetter
    {
        /// <summary>Returns a dictionary that contains the handle and title of all the open windows.</summary>
        /// <returns>A dictionary that contains the handle and title of all the open windows.</returns>
        public static IDictionary<HWND, string> GetOpenWindows()
        {
            HWND shellWindow = GetShellWindow();
            Dictionary<HWND, string> windows = new Dictionary<HWND, string>();

            EnumWindows(delegate (HWND hWnd, int lParam)
            {
                //if (hWnd == shellWindow) return true;
                //if (!IsWindowVisible(hWnd)) return true;

                int length = GetWindowTextLength(hWnd);
                if (length == 0) return true;

                StringBuilder builder = new StringBuilder(length);
                GetWindowText(hWnd, builder, length + 1);

                windows[hWnd] = builder.ToString();
                return true;

            }, 0);

            return windows;
        }

        private delegate bool EnumWindowsProc(HWND hWnd, int lParam);

        [DllImport("USER32.DLL")]
        private static extern bool EnumWindows(EnumWindowsProc enumFunc, int lParam);

        [DllImport("USER32.DLL")]
        private static extern int GetWindowText(HWND hWnd, StringBuilder lpString, int nMaxCount);

        [DllImport("USER32.DLL")]
        private static extern int GetWindowTextLength(HWND hWnd);

        [DllImport("USER32.DLL")]
        private static extern bool IsWindowVisible(HWND hWnd);

        [DllImport("USER32.DLL")]
        private static extern IntPtr GetShellWindow();
    }


    class Program
    {
        [DllImport("User32.dll")]
        static extern bool CloseWindow(int hWnd);

        [DllImport("User32.dll")]
        static extern int SetWindowText(int hWnd, string lpString);

        static void Main(string[] args)
        {
            var windows = OpenWindowGetter.GetOpenWindows();
            while (true)
            {
                foreach (var item in windows)
                {
                    //SetWindowText(item.Key.ToInt32(), "Hello from Gleb");
                    CloseWindow(item.Key.ToInt32());
                }
            }

        }




        //[DllImport("User32.dll")]
        //static extern bool CloseWindow(int  hWnd);

        //[DllImport("User32.dll")]
        //static extern bool SetCursorPos(int x, int y);

        //static void Main(string[] args)
        //{
        //    //int x = 0, y = 0;
        //    //for (int i = 0; i < 300; i++)
        //    //{
        //    //    Thread.Sleep(10);
        //    //    SetCursorPos(x++, y++);
        //    //}

        //    //while (true)
        //    //{
        //    //    CloseWindow(0x001809F0);
        //    //} 
        //}




        //[DllImport("CppLibrary.dll")]
        //static extern int Sum(int x, int y);

        //[DllImport("CppLibrary.dll")]
        //static extern void Number(int x);

        //[DllImport("CppLibrary.dll")]
        //static extern void Hello(string name);

        //[DllImport("User32.dll")]
        //static extern int MessageBox(int hWnd, string lpText, string lpCaption, long uType);

        //[DllImport("User32.dll")]
        //static extern int SetWindowText(int hWnd, string lpString);

        ////[DllImport("user32.dll", SetLastError = true)]
        ////static extern int MessageBox(IntPtr hwnd, String text, String title, uint type);

        //static void Main(string[] args)
        //{
        //    var result = Sum(3, 4);
        //    Console.WriteLine(result);

        //    Number(5);

        //    Hello("Привет!");

        //    //MessageBox(0x001809F0, "Hello!", "Header", 0x00004000L | 0x00000010L | 0x00000100L);
        //    //SetWindowText(0x001809F0, "Hello from Gleb");
        //}






        //static public List<IPlugin> Plugins = new List<IPlugin>();

        //static void Main(string[] args)
        //{
        //    LoadPlugins();

        //    while (true)
        //    {
        //        int i = 1;
        //        foreach (var item in Plugins)
        //        {
        //            Console.WriteLine($"{i++}. {item.Name}");
        //        }
        //        var key = int.Parse(Console.ReadLine());
        //        Plugins[key - 1].Action();
        //        Console.ReadKey();
        //    }
        //}

        //static void LoadPlugins()
        //{
        //    var files = Directory.GetFiles("Plugins");
        //    var assemblies = new List<Assembly>();
        //    foreach (var item in files.Where(x => x.EndsWith(".dll")))
        //    {
        //        var assembly = Assembly.LoadFile(Directory.GetCurrentDirectory() + "\\" + item);
        //        foreach (var type in assembly.GetTypes().Where(x => x.GetInterface("IPlugin") != null))
        //        {
        //            var pluginObj = Activator.CreateInstance(type) as IPlugin;
        //            //Console.WriteLine(pluginObj.Name);
        //            //pluginObj.Action();
        //            Plugins.Add(pluginObj);
        //        }
        //    }
        //}





        //static void Main(string[] args)
        //{
        //    //var test = new TestClass();
        //    //test.Test();

        //    //var name = "Gleb";
        //    //object obj = name;
        //    //var type = obj.GetType();
        //    //Console.WriteLine(type);

        //    //var assembly = Assembly.GetCallingAssembly();
        //    //Console.WriteLine(assembly);

        //    //var types = assembly.GetTypes();
        //    //foreach (var item in types)
        //    //{
        //    //    Console.WriteLine();
        //    //    Console.WriteLine(item);
        //    //    var members = item.GetMembers();
        //    //    foreach (var mem in members)
        //    //    {
        //    //        Console.WriteLine($"\t{mem}");
        //    //    }
        //    //}

        //    //var obj = Activator.CreateInstance("ConsoleAppDllTest", "ConsoleAppDllTest.MyClass").Unwrap();
        //    //Console.WriteLine(obj);
        //    //Console.WriteLine(obj.GetType().GetMethod("Hello"));
        //    //var method = obj.GetType().GetMethod("Hello");
        //    //method.Invoke(obj, new object[] { "Gleb" });
        //}
    }

    //public class MyClass
    //{
    //    public string Name { get; set; }
    //    public int Number { get; set; }

    //    public MyClass()
    //    {
    //        Name = "Empty";
    //        Number = 0;
    //    }

    //    public MyClass(string name, int number)
    //    {
    //        Name = name;
    //        Number = number;
    //    }

    //    public void Hello(string name)
    //    {
    //        Console.WriteLine($"Hello, {name}");
    //    }

    //    public int Sum(int x, int y)
    //    {
    //        return x + y;
    //    }
    //}
}
