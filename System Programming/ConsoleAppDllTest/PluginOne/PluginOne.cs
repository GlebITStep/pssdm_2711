﻿using IPluginDll;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PluginOneDll
{
    public class PluginOne : IPlugin
    {
        public string Name { get; set; } = "PluginOne";

        public void Action()
        {
            Console.WriteLine("Action from Plugin One");
        }
    }
}
