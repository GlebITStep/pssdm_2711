﻿using IPluginDll;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PluginTwoDll
{
    public class PluginTwo : IPlugin
    {
        public string Name { get; set; } = "PluginTwo";

        public void Action()
        {
            Console.WriteLine("Action from Plugin Two");
        }
    }
}
