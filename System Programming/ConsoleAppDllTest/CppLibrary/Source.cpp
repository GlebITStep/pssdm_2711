#include <iostream>
using namespace std;

extern "C" 
{
	__declspec(dllexport) void Hello(char* name)
	{
		cout << "Your name is " << name << endl;
	}

	__declspec(dllexport) void Number(int x)
	{
		cout << "Number is " << x << endl;
	}

	__declspec(dllexport) int Sum(int x, int y)
	{
		return x + y;
	}
}