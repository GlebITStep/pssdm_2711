﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AsyncAwait
{
    class Program
    {
        static void Main(string[] args)
        {
            Start();
            Loop();
        }

        static void Loop()
        {
            int x = 0;
            while (true)
            {
                Thread.Sleep(500);
                Console.WriteLine(x++);
            }
        }

        static async void Start()
        {
            //Console.WriteLine("Start");
            //var result = GetResultSync();
            //Console.WriteLine(result);
            //Console.WriteLine("Finish");


            //Console.WriteLine("Start");
            //GetResultThread();


            //Console.WriteLine("Start");
            //GetResultTask();


            Console.WriteLine("Start");
            int result = await GetResultAsync();
            Console.WriteLine(result);
            Console.WriteLine("Finish");
        }

        static Task<int> GetResultAsync()
        {
            return Task.Run(() =>
            {
                Thread.Sleep(2500);
                var result = 42;
                return result;
            });
        }

  
        static void GetResultTask()
        {
            Task.Run(() =>
            {
                Thread.Sleep(2500);
                var result = 42;
                Callback(result);
            });
        }


        static void GetResultThread()
        {
            Thread thread = new Thread(() =>
            {
                Thread.Sleep(2500);
                var result = 42;
                Callback(result);
            });
            thread.IsBackground = true;
            thread.Start();
        }


        static int GetResultSync()
        {
            Thread.Sleep(2500);
            var result = 42;
            return result;
        }


        private static void Callback(int result)
        {
            Console.WriteLine(result);
            Console.WriteLine("Finish");
        }
    }
}
