﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

namespace WpfAppSP1
{

    public partial class App : Application
    {
        static bool newMutex;
        static Mutex mutex = new Mutex(true, "Gleb", out newMutex);

        protected override void OnStartup(StartupEventArgs e)
        {
            if (!newMutex)
            {
                MessageBox.Show("Application si already running!");
                Environment.Exit(0);
            }
        }
    }
}
