﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

namespace WpfAppSP1
{
    class AppViewModel : ViewModelBase
    {
        private string fileName = @"C:\Skripnikov\Text1.txt";
        public string FileName
        {
            get { return fileName; }
            set { Set(ref fileName, value); }
        }

        private string result;
        public string Result
        {
            get { return result; }
            set { Set(ref result, value); }
        }

        private string apiResult;
        public string ApiResult
        {
            get { return apiResult; }
            set { Set(ref apiResult, value); }
        }

        private ObservableCollection<string> resultList = new ObservableCollection<string>();
        public ObservableCollection<string> ResultList
        {
            get { return resultList; }
            set { Set(ref resultList, value); }
        }

        private string apiUrl = @"https://www.bakubus.az/az/ajax/apiNew1";

        private RelayCommand startCommand;
        public RelayCommand StartCommand
        {
            get => startCommand ?? (startCommand = new RelayCommand(
                async () =>
                {
                    //Thread thread = new Thread(CountWords);
                    //thread.Start(FileName);

                    //ThreadPool.QueueUserWorkItem(CountWords, FileName);

                    //Task.Run(() => CountWords(FileName));

                    //try
                    //{
                    //    var count = await CountWordsAsync(FileName);
                    //    Result = count.ToString();
                    //    ResultList.Add(count.ToString());
                    //}
                    //catch (Exception ex)
                    //{
                    //    MessageBox.Show(ex.Message);
                    //}
                }
            ));
        }

        private RelayCommand apiCallCommand;
        public RelayCommand ApiCallCommand
        {
            get => apiCallCommand ?? (apiCallCommand = new RelayCommand(
                async () =>
                {
                    HttpClient httpClient = new HttpClient();
                    ApiResult = await httpClient.GetStringAsync(apiUrl);
                }
            ));
        }


        //Task CountWords2Async(string filename)
        //{
        //    return Task.Run(() =>
        //    {
        //        var text = File.ReadAllText(filename);
        //        var words = text.Split(' ');
        //        MessageBox.Show(words.Length.ToString());
        //    });
        //}

        Task<int> CountWordsAsync(string filename)
        {
            return Task.Run(() =>
            {
                var text = File.ReadAllText(filename);
                var words = text.Split(' ');
                return words.Length;
            });
        }

        int CountWords(string filename)
        {
            var text = File.ReadAllText(filename);
            var words = text.Split(' ');
            return words.Length;
        }

        //void CountWords(object data)
        //{
        //    var filename = data as string;
        //    var text = File.ReadAllText(filename);
        //    var words = text.Split(' ');
        //    Result = words.Length.ToString();

        //    Application.Current.Dispatcher.Invoke(() =>
        //    {
        //        ResultList.Add(words.Length.ToString());
        //    });
        //}
    }
}
