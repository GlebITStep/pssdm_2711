﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ThreadsSynchronization
{
    class Program
    {
        static void Main(string[] args)
        {
            int x = 0;
            object locker = new object();
            Mutex mutex = new Mutex();
            Semaphore semaphore = new Semaphore(3, 3);

            Task[] tasks = new Task[2];
            for (int i = 0; i < tasks.Length; i++)
            {
                tasks[i] = Task.Run(() =>
                {
                    for (int j = 0; j < 1000000; j++)
                    {
                        //semaphore.WaitOne();
                        //x++;
                        //semaphore.Release();

                        //mutex.WaitOne();
                        //x++;
                        //mutex.ReleaseMutex();

                        //lock (locker)
                        //{
                        //    x++;
                        //}

                        //Monitor.Enter(locker);
                        //x++;
                        //Monitor.Exit(locker);

                        //Interlocked.Increment(ref x);

                        //x++;
                    }
                });
            }

            Task.WaitAll(tasks);
            Console.WriteLine(x);
        }
    }
}
