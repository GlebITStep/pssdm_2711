﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
//using System.Windows.Forms;

namespace ConsoleAppSP2
{
    class Program
    {
        static void Func()
        {
            Console.WriteLine("Test thread");
        }

        static void Main(string[] args)
        {
            //Thread thread = new Thread(new ThreadStart(Func));
            //Thread thread = new Thread(Func);


            //Thread thread = new Thread(() =>
            //{
            //    Thread.Sleep(1000);
            //    //Console.WriteLine("Test thread");
            //    MessageBox.Show("Thread test");
            //});
            //thread.IsBackground = false;
            //Console.WriteLine("Start");
            //thread.Start();
            //Console.WriteLine("Finish");


            //Console.WriteLine("Start");
            //new Thread(() =>
            //{
            //    Thread.Sleep(1000);
            //    Console.WriteLine("Test thread");
            //}).Start();
            ////Thread.Sleep(10);
            //Console.WriteLine("Finish");


            //Thread thread = new Thread(CountWords);
            //Console.WriteLine("Start First Thread");
            //thread.Start(@"C:\Skripnikov\Text1.txt");
            //Console.WriteLine("Start Second Thread");
            //thread.Start(@"C:\Skripnikov\Text2.txt");
            //Console.WriteLine("Finish");


            //Console.WriteLine("Start First Thread");
            //ThreadPool.QueueUserWorkItem(CountWords, @"C:\Skripnikov\Text1.txt");
            //Console.WriteLine("Finish");
            //Console.ReadKey();



            //Console.WriteLine("Start First Thread");
            //ThreadPool.QueueUserWorkItem(param => 
            //{
            //    var filename = param as string;
            //    var text = File.ReadAllText(filename);
            //    var words = text.Split(' ');
            //    Console.WriteLine(words.Length);
            //}, @"C:\Skripnikov\Text1.txt");
            //Console.WriteLine("Finish");
            //Console.ReadKey();



            //Console.WriteLine("Start");
            //Task.Run(() =>
            //{
            //    var filename = @"C:\Skripnikov\Text1.txt";
            //    var text = File.ReadAllText(filename);
            //    var words = text.Split(' ');
            //    Console.WriteLine(words.Length);
            //});
            //Console.WriteLine("Finish");
            //Console.ReadKey();




            //Console.WriteLine("Start");
            //Task.Run(() => CountWords(@"C:\Skripnikov\Text1.txt"));
            //Console.WriteLine("Finish");
            //Console.ReadKey();


            //var timer = new Timer(param =>
            //{
            //    Console.WriteLine("Hello!");
            //}, null, 0, 1000);
            //Console.WriteLine("Start");
            //Console.WriteLine("Finish");
            //Console.ReadKey();



            //Console.WriteLine("Start");
            //Task.Run(() =>
            //{
            //    while (true)
            //    {
            //        Console.WriteLine("Hello!");
            //        Thread.Sleep(1000);
            //    }
            //});
            //Console.WriteLine("Finish");
            //Console.ReadKey();



            //var timer = new Timer(CountWords, @"C:\Skripnikov\Text2.txt", 0, 1000);
            //Console.WriteLine("Start");
            //Console.WriteLine("Finish");
            //Console.ReadKey();



            //Console.WriteLine("Start");
            //Task.Run(() =>
            //{
            //    while (true)
            //    {
            //        CountWords(@"C:\Skripnikov\Text2.txt");
            //        Thread.Sleep(1000);
            //    }
            //});
            //Console.WriteLine("Finish");
            //Console.ReadKey();




            Console.WriteLine("Start");
            Task.Run(() =>
            {
                try
                {
                    int x = 0;
                    Console.WriteLine(5 / x);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            });
            Console.WriteLine("Finish");
            Console.ReadKey();

            //async - await
        }

        static void CountWords(object data)
        {
            var filename = data as string;
            var text = File.ReadAllText(filename);
            var words = text.Split(' ');
            Console.WriteLine(words.Length);
        }
    }
}
