﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleAppSP1
{
    class Program
    {
        static void Main(string[] args)
        {
            //Process.Start("notepad++", "Text.txt");

            foreach (var item in Process.GetProcesses())
            {
                try
                {
                    Console.Write($"{item.Id}\t " +
                        $"| {item.ProcessName} " +
                        $"| {item.PriorityClass} " +
                        $"| {item.PagedMemorySize64}" +
                        $"| {item.TotalProcessorTime}\n");
                }
                catch (Exception) { }
            }

            //var time = new TimeSpan();
            //foreach (var item in Process.GetProcesses())
            //{
            //    try
            //    {
            //        time += item.TotalProcessorTime;
            //    }
            //    catch (Exception) { }
            //}
            //Console.WriteLine(time);

            //var process = Process.GetProcessesByName("devenv").FirstOrDefault();
            //foreach (var item in process.Modules)
            //{
            //    Console.WriteLine(item);
            //}
        }
    }
}
