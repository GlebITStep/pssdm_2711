﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace Server
{
    class User
    {
        public string Username { get; set; }
        public StreamReader Reader { get; set; }
        public StreamWriter Writer { get; set; }
        public TcpClient Client { get; set; }
    }

    class ServerTCP
    {
        static List<User> clients = new List<User>();

        static void Main(string[] args)
        {
            IPEndPoint endPoint = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 8080);

            TcpListener server = new TcpListener(endPoint);
            server.Start();

            Console.WriteLine("Server started...");
            while (true)
            {
                TcpClient client = server.AcceptTcpClient();

                Task.Run(() =>
                {
                    using (var reader = new StreamReader(client.GetStream()))
                    {
                        var username = "";
                        var connected = true;

                        while (connected)
                        {
                            try
                            {
                                var json = reader.ReadLine();
                                var msg = JsonConvert.DeserializeObject<Message>(json);

                                switch (msg.Type)
                                {
                                    case "Connect":
                                        Console.WriteLine($"User {msg.Data} connected!");
                                        clients.Add(new User
                                        {
                                            Username = msg.Data,
                                            Client = client,
                                            Writer = new StreamWriter(client.GetStream())
                                        });
                                        username = msg.Data;
                                        break;

                                    case "Message":
                                        Console.WriteLine($"{username}: {msg.Data}");
                                        BroadcastMessage(json);
                                        break;

                                    case "Disconnect":
                                        clients.Remove(clients.FirstOrDefault(x => x.Username == msg.Data));
                                        Console.WriteLine($"User {msg.Data} disconnected!");
                                        connected = false;
                                        break;

                                    default:
                                        break;
                                }
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine(ex.Message);
                                clients.Remove(clients.FirstOrDefault(x => x.Username == username));
                                Console.WriteLine($"User {username} disconnected!");
                                break;
                            }
                        }
                    }
                });
            }

        }

        static void BroadcastMessage(string msg)
        {
            foreach (var client in clients)
            {
                client.Writer.WriteLine(msg);
                client.Writer.Flush();
            }
        }

        //static void Main(string[] args)
        //{
        //    IPEndPoint endPoint = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 8080);

        //    Socket socket = new Socket(
        //        AddressFamily.InterNetwork,
        //        SocketType.Stream,
        //        ProtocolType.Tcp); 

        //    socket.Bind(endPoint);

        //    socket.Listen(10);

        //    Console.WriteLine("Server started...");
        //    while (true)
        //    {
        //        var clientSocket = socket.Accept();
        //        Console.WriteLine("Client connected...");

        //        Task.Run(() =>
        //        {
        //            byte[] buffer = new byte[65536];
        //            while (true)
        //            {
        //                try
        //                {
        //                    var size = clientSocket.Receive(buffer);
        //                    var message = Encoding.Unicode.GetString(buffer, 0, size);
        //                    Console.WriteLine(message);
        //                }
        //                catch (Exception ex)
        //                {
        //                    Console.WriteLine(ex.Message);
        //                    break;
        //                }
        //            }
        //        });
        //    }

        //}
    }
}
