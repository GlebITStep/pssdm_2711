﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfClient
{
    class Message
    {
        public string Type { get; set; }
        public string Data { get; set; }
    }
}
