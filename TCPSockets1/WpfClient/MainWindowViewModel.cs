﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace WpfClient
{
    class MainWindowViewModel : ViewModelBase
    {
        private int port = 8080;
        public int Port { get => port; set => Set(ref port, value); }

        private string ip = "127.0.0.1";
        public string Ip { get => ip; set => Set(ref ip, value); }

        private string username = "Gleb";
        public string Username { get => username; set => Set(ref username, value); }

        private string message;
        public string Message { get => message; set => Set(ref message, value); }

        private bool connected = false;
        public bool Connected { get => connected; set => Set(ref connected, value); }

        public ObservableCollection<string> Messages { get; set; } = new ObservableCollection<string>();

        TcpClient client;
        StreamWriter writer;
        StreamReader reader;
        

        private void RecieveMessages()
        {
            Task.Run(() =>
            {
                while (Connected)
                {
                    var msg = reader.ReadLine();

                    Application.Current.Dispatcher.Invoke(() =>
                    {
                        Messages.Add(msg);
                    });
                }
            });
        }

        private RelayCommand connectCommand;
        public RelayCommand ConnectCommand
        {
            get => connectCommand ?? (connectCommand = new RelayCommand(
                () =>
                {
                    client = new TcpClient();

                    IPEndPoint endPoint = new IPEndPoint(IPAddress.Parse(Ip), Port);
                    client.Connect(endPoint);
                    Connected = true;

                    writer = new StreamWriter(client.GetStream());
                    writer.AutoFlush = true;

                    var msg = new Message { Type = "Connect", Data = Username };
                    var json = JsonConvert.SerializeObject(msg);
                    writer.WriteLine(json);

                    reader = new StreamReader(client.GetStream());

                    RecieveMessages();
                }
            ));
        }

        private RelayCommand disconnectCommand;
        public RelayCommand DisconnectCommand
        {
            get => disconnectCommand ?? (disconnectCommand = new RelayCommand(
                () =>
                {
                    Connected = false;

                    var msg = new Message { Type = "Disconnect", Data = Username };
                    var json = JsonConvert.SerializeObject(msg);
                    writer.WriteLine(json);

                    writer.Dispose();
                    reader.Dispose();
                    client.Dispose();
                }
            ));
        }

        private RelayCommand sendCommand;
        public RelayCommand SendCommand
        {
            get => sendCommand ?? (sendCommand = new RelayCommand(
                () =>
                {
                    var msg = new Message { Type = "Message", Data = Message };
                    var json = JsonConvert.SerializeObject(msg);
                    writer.WriteLine(json);

                    Message = "";
                }
            ));
        }
    }
}
