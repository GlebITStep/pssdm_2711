﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace Client
{
    class ClientTCP
    {
        static void Main(string[] args)
        {
            IPEndPoint endPoint = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 8080);

            TcpClient client = new TcpClient();
            client.Connect(endPoint);

            using (var writer = new StreamWriter(client.GetStream()))
            {
                writer.AutoFlush = true;
                while (true)
                {
                    var message = Console.ReadLine();
                    writer.WriteLine(message);
                    //writer.Flush();
                }
            }
        }


        //static void Main(string[] args)
        //{
        //    IPEndPoint endPoint = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 8080);

        //    Socket socket = new Socket(
        //        AddressFamily.InterNetwork,
        //        SocketType.Stream,
        //        ProtocolType.Tcp);

        //    socket.Connect(endPoint);

        //    while (true)
        //    {
        //        var data = Console.ReadLine();
        //        socket.Send(Encoding.Unicode.GetBytes(data));
        //    }
        //}
    }
}
