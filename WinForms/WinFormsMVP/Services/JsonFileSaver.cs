﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WinFormsMVP.Model;

namespace WinFormsMVP.Services
{
    class JsonFileSaver : IFileSaver
    {
        public IEnumerable<Contact> Load(string filename)
        {
            var json = File.ReadAllText(filename);
            var list = JsonConvert.DeserializeObject<List<Contact>>(json);
            return list;
        }

        public void Save(IEnumerable<Contact> contacts, string filename)
        {
            var json = JsonConvert.SerializeObject(contacts);
            File.WriteAllText(filename, json);
        }
    }
}
