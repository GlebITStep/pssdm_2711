﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WinFormsMVP.Model;

namespace WinFormsMVP.Services
{
    public interface IFileSaver
    {
        void Save(IEnumerable<Contact> contacts, string filename);
        IEnumerable<Contact> Load(string filename);
    }
}
