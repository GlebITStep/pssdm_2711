﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WinFormsMVP.Model;

namespace WinFormsMVP.Services
{
    public interface IContactsDataStorage
    {
        void Add(Contact contact);
        void Remove(Contact contact);
        IEnumerable<Contact> GetContacts();
        void SetContacts(IEnumerable<Contact> contacts);
    }
}
