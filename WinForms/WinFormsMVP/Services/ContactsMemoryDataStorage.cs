﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using WinFormsMVP.Model;

namespace WinFormsMVP.Services
{
    public class ContactsMemoryDataStorage : IContactsDataStorage
    {
        public List<Contact> Contacts { get; set; } = new List<Contact>();

        public IEnumerable<Contact> GetContacts()
        {
            return Contacts;
        }

        public void SetContacts(IEnumerable<Contact> contacts)
        {
            Contacts = new List<Contact>(contacts);
        }

        public void Add(Contact contact)
        {
            if (String.IsNullOrWhiteSpace(contact.Name) ||
                String.IsNullOrWhiteSpace(contact.Surname) ||
                String.IsNullOrWhiteSpace(contact.Phone))
            {
                throw new Exception("Все поля должны быть заполнены!");
            }
            else if (!(new Regex("^[+]?[0-9]{7,16}$").IsMatch(contact.Phone)))
            {
                throw new Exception("Телефон введен некорректно!");
            }
            else
            {
                Contacts.Add(contact);                
            }
        }

        public void Remove(Contact contact)
        {
            Contacts.Remove(contact);
        }
    }
}
