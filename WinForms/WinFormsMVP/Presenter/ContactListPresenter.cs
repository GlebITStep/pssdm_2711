﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using WinFormsMVP.Model;
using WinFormsMVP.Services;
using WinFormsMVP.View;

namespace WinFormsMVP.Presenter
{
    public class ContactListPresenter
    {
        public ContactListView View { get; set; }
        public IContactsDataStorage Storage { get; set; } = new ContactsMemoryDataStorage();
        public IFileSaver Saver { get; set; } = new JsonFileSaver();

        public void AddContact(Contact contact)
        {
            Storage.Add(contact);
            View.UpdateList(Storage.GetContacts());
            View.Clear();
        }

        public void DeleteContact(Contact contact)
        {
            Storage.Remove(contact);
            View.UpdateList(Storage.GetContacts());
        }

        public void ExportContacts(string filename)
        {
            Saver.Save(Storage.GetContacts(), filename);
        }

        public void ImportContacts(string filename)
        {
            Storage.SetContacts(Saver.Load(filename));
            View.UpdateList(Storage.GetContacts());
        }
    }
}
