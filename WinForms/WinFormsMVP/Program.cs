﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using WinFormsMVP.Presenter;
using WinFormsMVP.View;

namespace WinFormsMVP
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            var contactListPresenter = new ContactListPresenter();
            var contactListView = new ContactListView(contactListPresenter);
            contactListPresenter.View = contactListView;

            Application.Run(contactListView);
        }
    }
}
