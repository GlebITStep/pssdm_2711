﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WinFormsMVP.Model;
using WinFormsMVP.Presenter;

namespace WinFormsMVP.View
{
    public partial class ContactListView : Form
    {
        public ContactListPresenter Presenter { get; set; }

        public ContactListView(ContactListPresenter presenter)
        {
            InitializeComponent();
            Presenter = presenter;
        }

        public void UpdateList(IEnumerable<Contact> contacts)
        {
            listBoxContacts.DataSource = null;
            listBoxContacts.DataSource = contacts;
        }

        public void Clear()
        {
            textBoxName.Text = "";
            textBoxSurname.Text = "";
            textBoxPhone.Text = "";
        }

        private void OnAddClick(object sender, EventArgs e)
        {
            try
            {
                Presenter.AddContact(new Contact
                {
                    Name = textBoxName.Text,
                    Surname = textBoxSurname.Text,
                    Phone = textBoxPhone.Text
                });
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void OnDeleteClick(object sender, EventArgs e)
        {
            var selectedContact = listBoxContacts.SelectedItem as Contact;
            if (selectedContact == null)
                MessageBox.Show("Вы не выбрали контакт!", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error); 
            else 
                Presenter.DeleteContact(selectedContact);
        }

        private void OnMenuExportClick(object sender, EventArgs e)
        {
            var dialog = new SaveFileDialog();
            dialog.Filter = "*JSON Files (*.json)|*.json";
            dialog.ShowDialog();
            var filename = dialog.FileName;
            Presenter.ExportContacts(filename);
        }

        private void OnMenuImportClick(object sender, EventArgs e)
        {
            var dialog = new OpenFileDialog();
            dialog.Filter = "*JSON Files (*.json)|*.json";
            dialog.ShowDialog();
            var filename = dialog.FileName;
            Presenter.ImportContacts(filename);
        }
    }
}
