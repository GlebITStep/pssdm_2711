// let arr = new Array();
// let arr = [1, 'gleb', false];
// console.log(arr);



// let arr = [11, 22, 33, 44, 55];
// arr[8] = 99;
// arr['name'] = 'Gleb';
// arr.age = 25;
// console.log(arr);

// // for (let i = 0; i < arr.length; i++) {
// // 	console.log(arr[i]);	
// // }

// // for (const item of arr) {
// // 	console.log(item);
// // }

// // for (const key in arr) {
// // 	console.log(arr[key]);	
// // }

// // arr.forEach(function(item, index, array) {
// // 	console.log(index + " " + item);	
// // 	console.log(array);
// // });

// // arr.forEach(item => {
// // 	console.log(item);	
// // });


// let arr = [11, 22, 33, 44, 55];
// // arr[arr.length - 1] = 99;
// // arr.push(88);
// // arr.push(99);
// // arr.pop();
// // arr.shift();
// // arr.unshift(77);

// // arr.splice(2, 2);
// // arr.splice(-2, 2);
// // arr.splice(-1, 1);
// // arr.splice(0, 1);
// // arr.splice(3, 1, 66, 77, 88);

// // delete arr[2];
// // arr[2] = undefined;

// for (const item of arr) {
// 	console.log(item);
// }



let arr = [99, 22, 44, 22, 55];
console.log(arr.indexOf(22));
console.log(arr.lastIndexOf(22));

arr.sort();
console.log(arr);

arr.sort((a, b) => {
	return b - a;
});
console.log(arr);

var newArr = arr.filter(x => x % 2 == 0);
console.log(newArr);
