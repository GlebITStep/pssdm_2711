var app = new Vue({
	el: '#app',

	data: {
		message: 'My name is Gleb',
		counter: 0
	},

	methods: {
		onCountClick: function() {
			this.counter++;
		}
	}
})

var appCalc = new Vue({
	el: '#app-calc',

	data: {
		number1: 2,
		number2: 3,
		result: 0
	},

	methods: {
		calcResult: function() {
			this.result = +this.number1 + +this.number2;
		}
	}
})

var appCalc = new Vue({
	el: '#app-movies',

	data: {
		apiUrl: 'http://www.omdbapi.com',
		apiKey: '2c9d65d5',
		searchText: 'Matrix',
		movies: [],
		selectedMovie: null,
		fullMovieInfo: null
	},

	methods: {
		onSearch: async function() {
			let response = await fetch(`${this.apiUrl}?apikey=${this.apiKey}&s=${this.searchText}`);
			let data = await response.json();
			this.movies = data.Search;
			this.searchText = '';
		},

		onMovieClick: async function(movie) {
			this.selectedMovie = movie;
			let movieId = movie.imdbID;	
			let response = await fetch(`${this.apiUrl}?apikey=${this.apiKey}&i=${movieId}`);
			let data = await response.json();
			this.fullMovieInfo = data;	
		}
	}
})

