//http://www.omdbapi.com/?apikey=2c9d65d5&s=Matrix

let resultDiv = document.querySelector('#result');
let modalElem = document.querySelector('#detailsModal');
let searchForm = document.forms.searchForm;
let apiUrl = 'http://www.omdbapi.com/';
let apiKey = '2c9d65d5';
let searchResult = [];
let page = 1;

searchForm.addEventListener('submit', onSearch);
window.addEventListener('scroll', onScroll);

function onScroll() {
	let windowHeight = document.documentElement.clientHeight; //700
	if (document.body.scrollHeight - (window.scrollY + windowHeight) < 0) {  //2000 - (100 + 700)
		page++;
		nextSearch(page);
	}
}

function nextSearch(page) {
	let searchText = searchForm.search.value;

	let httpRequest = new XMLHttpRequest();	
	httpRequest.onload = onApiResponse;
	httpRequest.open('GET', `${apiUrl}?apikey=${apiKey}&s=${searchText}&page=${page}`);
	httpRequest.send();	
}

function onSearch() {
	event.preventDefault();

	let searchText = searchForm.search.value;

	let httpRequest = new XMLHttpRequest();	
	httpRequest.onload = onApiResponse;
	httpRequest.open('GET', `${apiUrl}?apikey=${apiKey}&s=${searchText}`);
	httpRequest.send();	
	// searchForm.reset();
	resultDiv.innerHTML = "";
}

function onApiResponse() {
	let data = JSON.parse(event.target.response);
	searchResult =  data.Search;	
	for (const item of searchResult) {
		createMovie(item);
	}
}

function createMovie(movie) {
	let template = `
	<div class="col-md-4 mb-4">
		<div class="card">
			<img onerror="onImgError()" src="${movie.Poster}" class="card-img-top" alt="...">
			<div class="card-body">
				<h5 class="card-title">${movie.Title}</h5>
				<p class="card-text">${movie.Year}</p>
				<button data-id="${movie.imdbID}" onclick="onDetailsClick()" type="button" class="btn btn-primary">Details...</button>
			</div>
		</div>
	</div>`;

	resultDiv.insertAdjacentHTML('beforeend', template);
}

function onDetailsClick() {
	let id = event.target.getAttribute('data-id');
	console.log(id);

	let httpRequest = new XMLHttpRequest();	
	httpRequest.onload = onDetailsResponse;
	httpRequest.open('GET', `${apiUrl}?apikey=${apiKey}&i=${id}`);
	httpRequest.send();	
}

function onDetailsResponse() {
	let data = JSON.parse(event.target.response);	
	modalElem.querySelector('.modal-title').innerText = data.Title;
	modalElem.querySelector('.modal-body').innerText = data.Plot;
	$('#detailsModal').modal('toggle');
}

function onImgError() {
	event.target.src = 'placeholder.png';
}

// function createMovie(movie) {
// 	let colElem = document.createElement('div');
// 	colElem.className = 'col-md-4 mb-4';
	
// 	let titleElem = document.createElement('h5');
// 	titleElem.innerText = movie.Title;

// 	colElem.appendChild(titleElem);

// 	resultDiv.appendChild(colElem);
// }


