$(function() {

	const apikey = '835492c0eab300994ec658dfb16ad305';
	const apiUrl = 'https://api.openweathermap.org/data/2.5/weather';
	let cities = [];
	
	loadCities();

	function loadCities() {
		let json = localStorage.getItem('cities'); //'["Baku","Odessa","Ganja","Krakow"]'
		if (json) {
			cities = JSON.parse(json);	
		}
		
		for (const city of cities) {
			addCity(city);
		}
	}
	
	$('#addCity').click(function() {
		let city = $('#cityName').val();
	
		cities.push(city);
		localStorage.setItem('cities', JSON.stringify(cities)); 
	
		addCity(city);
		$('#cityName').val('');
	});
	
	function addCity(cityName) {
		let template = `<li class="list-group-item">${cityName}<i class="fa fa-trash text-danger"></i></li>`;
		$('#cityList').append(template);
		$('#cityList > li').last().click(cityClick);
		$('#cityList > li').last().find('i').click(removeCity);
	}
	
	function removeCity() {
		let name = $(this).parent().text().trim();
		cities = cities.filter(x => x != name);
		localStorage.setItem('cities', JSON.stringify(cities));
	
		$(this).parent().remove();
	}
	
	function cityClick() {
		$('#cityList > li').removeClass('active');
		$(this).addClass('active');

		let city = $(this).text().trim();

		let params = { 
			appid: apikey, 
			q: city,
			units: 'metric'
		};
		$.get(apiUrl, params, function(data) {
			console.log(data);
			
			$.get('template.html', function(html) {
				let rendered = Mustache.render(html, data);
				$('#weather').html(rendered);
			});
			
		});
	}

	// function cityClickOld() {
	// 	let city = $(this).text().trim();
	// 	console.log(city);	

	// 	let params = { 
	// 		appid: apikey, 
	// 		q: city,
	// 		units: 'metric'
	// 	};
	// 	$.get(apiUrl, params, function(data) {
	// 		console.log(data);
	// 		$('#weather').attr('hidden', false);
	// 		$('#weatherName').text(data.name);
	// 		$('#weatherDescription').text(data.weather[0].description);
	// 		$('#weatherTemperature').text(data.main.temp);

	// 		// let template = $('#template').html();
	// 		// let rendered = Mustache.render(template, data);
	// 		// $('#weather').html(rendered);
	// 	});
	// }

});

