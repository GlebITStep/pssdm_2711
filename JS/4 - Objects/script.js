// var name = 'Test';

// let person = {
// 	name: "Gleb",
// 	age: 25,

// 	sayFunction: function() {
// 		console.log(`Hello from ${this.name}`);	
// 		console.log(this);	
// 	},

// 	sayArrow: () => {
// 		console.log(`Hello from ${this.name}`);	
// 		console.log(this);
// 	}
// };

// // console.log(person);
// person.sayFunction();
// person.sayArrow();




// let person1 = {
// 	name: "Gleb",
// 	age: 25,
// };
// person1.test = 'Test';

// let person2 = {
// 	name: "Ivan",
// 	age: 20,
// };



// function Person(name, age) {
// 	this.name = name;
// 	this.age = age;
// }
// let person = new Person('Gleb', 25);
// console.log(person);
// console.log(typeof(person));



// let date = new Date();
// console.log(date);



// let person = {
// 	name: 'Gleb',
// 	surname: 'Skripnikov',
// 	age: 25,

// 	fullnameFunc: function() {
// 		return `${this.name} ${this.surname}`;
// 	},

// 	get fullname() {
// 		return `${this.name} ${this.surname}`;
// 	},

// 	set fullname(fullnameStr) {
// 		let words = fullnameStr.split(' ');
// 		this.name = words[0];
// 		this.surname = words[1];
// 	}
// };
// person.test = true;

// console.log(person.name);
// console.log(person['name']);

// console.log(person.fullname);
// person.fullname = 'Ivan Ivanov';
// console.log(person.fullname);

// for (const key in person) {
// 	console.log(key);	
// }



// let x = 1;
// if (x == '1') {
// 	console.log('YES');
// } else {
// 	console.log('NO');	
// }