// npm i --save @types/jquery


//Plain JS
// document.querySelector('.box').addEventListener('click', function() {
// 	console.log('test');	
// });
// document.querySelector('.box').onclick = function() {
// 	console.log('test');	
// };
// let boxes = document.querySelectorAll('.box');
// for (const box of boxes) {
// 	box.addEventListener('click', function() {
// 		console.log('test');	
// 	});
// }


//jQuery

// $('.box').click(function() {
// 	console.log('test');	
// });

// $('#title').click(function() {
// 	// $('#content').hide();
// 	// $('#content').show();
// 	$('#content').toggle();
// });

// $('.header').click(function() {
// 	$('.content').toggle();
// });

$('.header').click(function() {
	$(this).next().toggle();
	$(this).toggleClass('active');
	// $(this).next().fadeToggle(1000);
	// $(this).next().fadeToggle(1000, function() {
	// 	console.log('done');	
	// });
	// $(this).next().slideToggle(500);
	// $(this).next().animate({transform: 'rotate(90deg)'});
	// $(this).next().animate({width: '200px', height: '400px'});
	// $(this).next().animate({'background-color': 'red'});
	// $(this).next()
	// 	.animate({width: '200px', height: '400px'})
	// 	.animate({'background-color': 'red'});
});

// $('.header').click(function() {
// 	// console.log($(this).text());
// 	// $(this).text('Hello!');
	
// 	// $(this).html('<button>Test</button>');

// 	// $(this).attr('title', 'Hello!');

// 	// $(this).css('color', 'red').css('font-size', '50px');

// 	// $(this).css({
// 	// 	'color': 'red',
// 	// 	'font-size': '20px'
// 	// }).text('Hello');

// 	console.log($(this).css('color'));	
// });

// $('#click').click(function() {
// 	console.log($('#name').val());
// 	$('#name').val('Gleb');
// });