// let xhttp = new XMLHttpRequest();
// xhttp.open('GET', 'http://www.omdbapi.com/?apikey=2c9d65d5&s=Matrix', false);
// xhttp.send();
// console.log('Test');
// console.log(xhttp.response);
// console.log('Test2');



// let xhttp = new XMLHttpRequest();
// xhttp.open('GET', 'http://www.omdbapi.com/?apikey=2c9d65d5&s=Matrix');
// xhttp.send();
// console.log('Test');
// setTimeout(function() {
// 	console.log(xhttp.response);
// }, 1000);
// console.log('Test2');



// let xhttp = new XMLHttpRequest();
// xhttp.open('GET', 'http://www.omdbapi.com/?apikey=2c9d65d5&s=Matrix');
// xhttp.send();
// xhttp.onload = function() {
// 	console.log(xhttp.response);	
// };
// console.log('Test');
// console.log('Test2');




// let check = false;

// let promise = new Promise(function(resolve, reject) {
// 	if (check) {
// 		resolve('Test');
// 	} else {
// 		reject('Some error!');
// 	}
// });

// promise.then(function(response) {
// 	console.log(response);	
// }).catch(function(error) {
// 	console.log(error);
// });





// let goodMood = true;

// let getMark = new Promise(function(resolve, reject) {
// 	if (goodMood) {
// 		setTimeout(function () {
// 			resolve(12);
// 		}, 2000);
// 	} else {
// 		setTimeout(function () {
// 			reject(1);
// 		}, 500);
// 	}
// });

// console.log('Begin');
// getMark.then(function(response) {
// 	console.log(`My mark is ${response}!`);	
// }).catch(function (error) {
// 	console.log(`My mark is ${error} :(`);	
// });
// console.log('End');





// function getMark(homework) {
// 	return new Promise(function(resolve, reject) {
// 		if (homework) {
// 			setTimeout(function () {
// 				resolve(12);
// 			}, 2000);
// 		} else {
// 			setTimeout(function () {
// 				reject(1);
// 			}, 500);
// 		}
// 	});
// }

// getMark(false).then(function(response) {
// 	console.log(`My mark is ${response}!`);	
// }).catch(function (error) {
// 	console.log(`My mark is ${error} :(`);	
// });





function httpGet(url) {
	return new Promise(function (resolve, reject) {
		let xhttp = new XMLHttpRequest();
		xhttp.open('GET', url, true);
		xhttp.send();
		xhttp.onload = function() {
			resolve(xhttp.response);	
		};
		xhttp.onerror = function() {
			reject('Error!');
		}
	});
};

// httpGet('http://www.omdbapi.com/?apikey=2c9d65d5&s=Matrix')
// .then(function(response) {
// 	console.log(response);
// }).catch(function(error) {
// 	console.log(error);	
// });

// httpGet('http://www.omdbapi.com/?apikey=2c9d65d5&s=Matrix')
// .then(function(response) {
// 	console.log(response);
// 	return httpGet('http://www.omdbapi.com/?apikey=2c9d65d5&s=Terminator');
// }).then(function(response) {
// 	console.log(response);
// 	return httpGet('http://www.omdbapi.com/?apikey=2c9d65d5&s=Avengers');
// }).then(function(response) {
// 	console.log(response);
// }).catch(function(error) {
// 	console.log(error);	
// });



// async function getMovies() {
// 	try {
// 		let result1 = await httpGet('http://www.omdbapi.com/?apikey=2c9d65d5&s=Matrix');
// 		console.log(result1);	
// 		let result2 = await httpGet('http://www.omdbapi.com/?apikey=2c9d65d5&s=Terminator');
// 		console.log(result2);	
// 		let result3 = await httpGet('http://www.omdbapi.com/?apikey=2c9d65d5&s=Avengers');
// 		console.log(result3);		
// 	} catch (error) {
// 		console.log(error);	
// 	}
// }

// console.log('Begin');
// getMovies();
// console.log('End');

function httpGetJson(url) {
	return new Promise(function (resolve, reject) {
		let xhttp = new XMLHttpRequest();
		xhttp.open('GET', url, true);
		xhttp.send();
		xhttp.onload = function() {
			resolve(JSON.parse(xhttp.response));	
		};
		xhttp.onerror = function() {
			reject('Error!');
		}
	});
};

async function getMoviePlot(movieName) {
	try {
		let searchResultData = await httpGetJson('http://www.omdbapi.com/?apikey=2c9d65d5&s=' + movieName)
		let movieId = searchResultData.Search[0].imdbID;
		let movieResultData = await httpGetJson('http://www.omdbapi.com/?apikey=2c9d65d5&i=' + movieId);
		console.log(movieResultData.Plot);	
	} catch (error) {
		console.log(error);	
	}
}

// getMoviePlot('Cucumber');





//1 - 'DONE'
//2 - 'DONE'
//3 - 'DONE'

// setTimeout(function() {
// 	console.log('Done!');	
// 	setTimeout(function() {
// 		console.log('Done!');	
// 		setTimeout(function() {
// 			console.log('Done!');	
// 		}, 3000);
// 	}, 2000);
// }, 1000);




function delay(time) {
	return new Promise(function(resolve, reject) {
		setTimeout(function() {
			resolve('Done');
		}, time);
	});
}

// delay(1000).then(function(response) {
// 	console.log(response);
// 	return delay(2000);
// }).then(function(response) {
// 	console.log(response);
// 	return delay(3000);
// }).then(function(response) {
// 	console.log(response);
// });



// async function test() {
// 	try {
// 		let response = await delay(1000);
// 		console.log(response);	
// 		response = await delay(2000);
// 		console.log(response);	
// 		response = await delay(3000);
// 		console.log(response);
// 	} catch (error) {
// 		console.log(error);	
// 	}	
// }

// test();









// function delay(time) {
// 	return new Promise(function (resolve, reject) {
// 		setTimeout(function() {
// 			resolve('Done!');
// 		}, time);
// 	})
// }

// // delay(1000).then(function(response) {
// // 	console.log(response);	
// // 	return delay(2000);
// // }).then(function(response) {
// // 	console.log(response);	
// // 	return delay(3000);
// // }).then(function(response) {
// // 	console.log(response);
// // })


// async function test() {
// 	let response = await delay(1000);
// 	console.log(response);
// 	response = await delay(2000);
// 	console.log(response);
// 	response = await delay(3000);
// 	console.log(response);
// }

// test();





// async function getData() {
// 	let response = await fetch('http://www.omdbapi.com/?apikey=2c9d65d5&s=Matrix', { method: 'POST' });
// 	let data = await response.json();
// 	console.log(data);	
// }

// getData();
