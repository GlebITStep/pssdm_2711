﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WpfApp2.Model;

namespace WpfApp2
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        public ObservableCollection<Product> List { get; set; } = new ObservableCollection<Product>();
        public string ConnectionString { get; set; }

        public MainWindow()
        {
            InitializeComponent();
            ConnectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            listBoxProducts.ItemsSource = List;
        }

        private void OnSearchClick(object sender, RoutedEventArgs e)
        {
            List.Clear();
            using (var connection = new SqlConnection(ConnectionString))
            {
                try
                {
                    connection.Open();
                    var query = "select * from Product";
                    var command = new SqlCommand(query, connection);
                    var reader = command.ExecuteReader();
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            var product = new Product();
                            product.Id = reader.GetInt32(0);
                            product.ProductName = reader.GetString(1);
                            product.SupplierId = reader.GetInt32(2);
                            product.UnitPrice = reader.GetDecimal(3);
                            product.Package = reader.GetString(4);
                            product.IsDiscounted = reader.GetBoolean(5);
                            List.Add(product);
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void OnProductSelected(object sender, MouseButtonEventArgs e)
        {
            if (listBoxProducts.SelectedIndex != -1)
            {
                var selected = listBoxProducts.SelectedItem;
                MessageBox.Show(selected.ToString());
            }
        }

        private void OnSumClick(object sender, RoutedEventArgs e)
        {
            using (var connection = new SqlConnection(ConnectionString))
            {
                try
                {
                    connection.Open();
                    var query = "select sum(UnitPrice) from Product";
                    var command = new SqlCommand(query, connection);
                    var result = command.ExecuteScalar();
                    MessageBox.Show(result.ToString());
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void OnAddClick(object sender, RoutedEventArgs e)
        {
            using (var connection = new SqlConnection(ConnectionString))
            {
                try
                {
                    connection.Open();
                    var query = "insert into Product values (@name, 1, 10, 'Box', 0)";
                    var command = new SqlCommand(query, connection);
                    command.Parameters.Add(new SqlParameter
                    {
                        ParameterName = "@name",
                        Value = textBoxInput.Text
                    });
                    var result = command.ExecuteNonQuery(); //insert update delete
                    OnSearchClick(null, null);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }
    }
}
